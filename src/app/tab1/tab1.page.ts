import { Component, OnInit } from '@angular/core';
import { QuotesService } from '../api/quotes.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {
  quote = '';

  constructor(private quotesService: QuotesService) {}

  ngOnInit() {
    this.randomQuote();
  }

  randomQuote() {
    this.quotesService.getAll()
      .subscribe((quotes) => {
        this.quote = quotes[Math.floor(Math.random() * quotes.length)];
      });
  }
}
