import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import Quotes from '../../assets/data/quotes.json';

@Injectable({
  providedIn: 'root'
})
export class QuotesService {

  constructor() { }

  getAll(): Observable<any> {
    return of(Quotes);
  }
}
