import { Component, OnInit } from '@angular/core';
import { QuotesService } from '../api/quotes.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit {
  quotes = [];

  constructor(
    private quotesService: QuotesService) {}

  ngOnInit() {
    this.loadQuotes();
  }

  showDetails() {
    
  }

  loadQuotes() {
    this.quotesService.getAll()
      .subscribe((quotes) => this.quotes = quotes);
  }
}
