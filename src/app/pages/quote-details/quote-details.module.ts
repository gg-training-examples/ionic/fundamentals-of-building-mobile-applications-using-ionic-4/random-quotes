import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { QuoteDetailsPageRoutingModule } from './quote-details-routing.module';

import { QuoteDetailsPage } from './quote-details.page';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule,
    CommonModule,
    FormsModule,
    IonicModule,
    QuoteDetailsPageRoutingModule
  ],
  declarations: [QuoteDetailsPage]
})
export class QuoteDetailsPageModule {}
