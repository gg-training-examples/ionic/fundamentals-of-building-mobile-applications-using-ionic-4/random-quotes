import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { QuotesService } from 'src/app/api/quotes.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-quote-details',
  templateUrl: './quote-details.page.html',
  styleUrls: ['./quote-details.page.scss']
})
export class QuoteDetailsPage implements OnInit {
  quote = {};

  constructor(
    public navController: NavController,
    private router: ActivatedRoute,
    private quotesService: QuotesService
  ) { }

  ngOnInit() {
    this.router.params.subscribe(params => {
      const id = params['id'];
      this.quotesService.getAll()
        .subscribe((quotes) => this.quote = quotes[id]);
    });
  }

  // back() {
  //   this.navController.back();
  // }
}
